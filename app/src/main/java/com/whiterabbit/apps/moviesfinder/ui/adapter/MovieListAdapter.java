package com.whiterabbit.apps.moviesfinder.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.whiterabbit.apps.moviesfinder.R;
import com.whiterabbit.apps.moviesfinder.model.Movie;
import com.whiterabbit.apps.moviesfinder.network.MovieServiceConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Tania Pinheiro
 *
 * Adapter that serves a movie list
 */
public class MovieListAdapter extends BaseAdapter {

    private final SimpleDateFormat dateParser;
    private final Calendar calendar;
    private final int fallbackColor;
    private Context context;
    private List<Movie> list;
    private LayoutInflater inflater;
    private String baseUrl;

    public MovieListAdapter(Context context, List<Movie> list) {
        this.context = context;
        this.list = list;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        baseUrl = MovieServiceConstants.IMAGES_END_POINT;
        dateParser = new SimpleDateFormat("yyyy-MM-dd");
        calendar = Calendar.getInstance();
        fallbackColor = context.getResources().getColor(R.color.primary_color_dark);
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public static class ViewHolder {

        public TextView title;
        public ImageView poster;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
            //if the view wasn't created yet, setup view
            if (rowView == null) {
                rowView = inflater.inflate(R.layout.item_row_movie_list, null);
                final ViewHolder viewHolder = new ViewHolder();
                viewHolder.title = (TextView) rowView.findViewById(R.id.movie_title);
                viewHolder.poster = (ImageView) rowView.findViewById(R.id.movie_backdrop);
                rowView.setTag(viewHolder);
            }

            //populate view with data
            final ViewHolder holder = (ViewHolder) rowView.getTag();
            Movie movie = (Movie) getItem(position);
            holder.title.setText(movie.getTitle() + getYear(movie.getRelease_date()));

            //load image and set the title background with the dark muted color retrieved from the image
            Picasso.with(context).load(baseUrl + movie.getBackdrop_path()).into(holder.poster, new Callback() {
                @Override
                public void onSuccess() {
                    Bitmap bitmap = ((BitmapDrawable) holder.poster.getDrawable()).getBitmap();
                    Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
                        @Override
                        public void onGenerated(Palette palette) {
                            holder.title.setBackgroundColor(palette.getDarkMutedColor(fallbackColor));
                        }
                    });
                }

                @Override
                public void onError() {
                    //there was an error, setting fallback color
                    holder.poster.setBackgroundColor(fallbackColor);

                }
            });

        return rowView;
    }

    /**
     * @params: the date in a yyyy-MM-dd format
     * @returns: the year of the given date
     */
    private String getYear(String releaseDate) {
        try {
            Date date = dateParser.parse(releaseDate);
            calendar.setTime(date);
            return " ("  + calendar.get(Calendar.YEAR) + ")";
        } catch (ParseException e) {
            return "";
        }

    }


}

package com.whiterabbit.apps.moviesfinder.model;

/**
 * Created by Tania Pinheiro
 *
 * This class (POJO) represents a cast member
 */
public class Cast
{
    private String id;

    private String name;

    private String profile_path;

    private String character;

    public String getId ()
    {
        return id;
    }

    public String getName ()
    {
        return name;
    }

    public String getProfile_path ()
    {
        return profile_path;
    }

    public String getCharacter ()
    {
        return character;
    }

}

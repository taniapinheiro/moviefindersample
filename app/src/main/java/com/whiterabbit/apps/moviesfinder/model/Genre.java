package com.whiterabbit.apps.moviesfinder.model;

/**
 * Created by Tania Pinheiro
 *
 * This class (POJO) represents a movie genre
 */
public class Genre
{
    private String id;

    private String name;

    public String getId ()
    {
        return id;
    }

    public String getName ()
    {
        return name;
    }

}
package com.whiterabbit.apps.moviesfinder.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.whiterabbit.apps.moviesfinder.AppApplication;
import com.whiterabbit.apps.moviesfinder.R;
import com.whiterabbit.apps.moviesfinder.model.Movie;
import com.whiterabbit.apps.moviesfinder.model.ResultsMoviePage;
import com.whiterabbit.apps.moviesfinder.ui.adapter.MovieListAdapter;
import com.whiterabbit.apps.moviesfinder.ui.EndlessScrollListener;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Tania Pinheiro
 *
 * This activity is the entry point of the app
 * Contains a gridview to showcase the most popular movies.
 * Upon scrolling more movies are loaded.
 */


public class MovieActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private GridView mGridView;
    private List<Movie> movieList;
    private MovieListAdapter mAdapter;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        //setup toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //setup views
        movieList = new ArrayList<Movie>();
        mAdapter = new MovieListAdapter(this,movieList);
        mGridView = (GridView) findViewById(R.id.gridview);

        mGridView.setOnItemClickListener(this);
        mGridView.setAdapter(mAdapter);
        mGridView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                //on scroll request next page
                loadDataFromApi(page);

            }

        });

        //request first page of data
        loadDataFromApi(1);
    }



    private void loadDataFromApi(final int page) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //request a page of movies and add the result to the list of movies
                AppApplication.getMoviesService().getPopularMovies(page, new Callback<ResultsMoviePage>() {
                    @Override
                    public void success(ResultsMoviePage resultsMoviePage, Response response) {
                        //request succeeded, adding all the new results to the movie list
                        movieList.addAll(resultsMoviePage.getMovies());

                        //notify the changes to the ui
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //there was an error, notifying the ui
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MovieActivity.this, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        }).start();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // get the movie from the view that was touched
        Movie movie = ((Movie) mAdapter.getItem(position));

        //create intent to start MovieDetailsActivity
        Intent intent = new Intent(this, MovieDetailsActivity.class);
        intent.putExtra(Intent.EXTRA_UID, movie.getId());
        startActivity(intent);
    }

}

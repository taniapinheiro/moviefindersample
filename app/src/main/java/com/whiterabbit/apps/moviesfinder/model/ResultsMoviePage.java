package com.whiterabbit.apps.moviesfinder.model;

import java.util.List;

/**
 * Created by Tania Pinheiro
 *
 *  This class (POJO) represents the request result for a page of movies
 */
public class ResultsMoviePage
{
    private List<Movie> results;

    private String page;

    private String total_pages;

    private String total_results;

    public List<Movie> getMovies ()
    {
        return results;
    }


    public String getPage ()
    {
        return page;
    }

    public void setPage (String page)
    {
        this.page = page;
    }

    public String getTotal_pages ()
    {
        return total_pages;
    }

    public void setTotal_pages (String total_pages)
    {
        this.total_pages = total_pages;
    }

    public String getTotal_results ()
    {
        return total_results;
    }

    public void setTotal_results (String total_results)
    {
        this.total_results = total_results;
    }
}
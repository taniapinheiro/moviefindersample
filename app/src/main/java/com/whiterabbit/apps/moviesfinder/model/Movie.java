package com.whiterabbit.apps.moviesfinder.model;

/**
 * Created by Tania Pinheiro
 *
 * This class (POJO) represents a movie
 */

public class Movie {

    private String backdrop_path;

    private Genre[] genres;

    private String status;

    private String runtime;

    private String homepage;

    private String id;

    private String title;

    private String overview;

    private String release_date;

    private String tagline;


    public Genre[] getGenres ()
    {
        return genres;
    }

    public String getRuntime ()
    {
        return runtime;
    }

    public String getHomepage ()
    {
        return homepage;
    }

    public String getOverview ()
    {
        return overview;
    }

    public String getTagline ()
    {
        return tagline;
    }

    public String getId ()
    {
        return id;
    }

    public String getTitle ()
    {
        return title;
    }

    public String getBackdrop_path ()
    {
        return backdrop_path;
    }

    public String getRelease_date ()
    {
        return release_date;
    }

}
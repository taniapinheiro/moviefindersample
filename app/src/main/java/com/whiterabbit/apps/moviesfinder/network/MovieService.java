package com.whiterabbit.apps.moviesfinder.network;

import com.whiterabbit.apps.moviesfinder.model.Movie;
import com.whiterabbit.apps.moviesfinder.model.ResultsMovieCredits;
import com.whiterabbit.apps.moviesfinder.model.ResultsMoviePage;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Tania Pinheiro
 *
 * This service handles all the Retrofit requests
 */
public interface MovieService {

    @GET("/movie/popular")
    void getPopularMovies(@Query("page") int page, Callback<ResultsMoviePage> callback);

    @GET("/movie/{id}")
    void getMovie(@Path("id") String movieId, Callback<Movie> callback);

    @GET("/movie/{id}/credits")
    ResultsMovieCredits getCredits(@Path("id") String movieId);

    @GET("/movie/{id}/similar")
    ResultsMoviePage getSimilarMovies(@Path("id") String movieId, @Query("page") int page);

}

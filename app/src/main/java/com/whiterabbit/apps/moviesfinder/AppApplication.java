package com.whiterabbit.apps.moviesfinder;

import android.app.Application;

import com.whiterabbit.apps.moviesfinder.network.MovieService;
import com.whiterabbit.apps.moviesfinder.network.MovieServiceConstants;
import com.whiterabbit.apps.moviesfinder.network.MovieServiceInterceptor;

import retrofit.RestAdapter;

/**
 * Created by Tania Pinheiro on 04.12.14.
 */
public class AppApplication extends Application {

    private static AppApplication app;
    private static MovieService service;

    // API Key: d02f41c8b1c3d3a5012f455749fb20ef

    public static AppApplication getInstance() {
        return app;
    }

    public static MovieService getMoviesService() {
        return service;
    }


    public final void onCreate() {
        super.onCreate();
        app = this;
        //Retrofit setup
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(MovieServiceConstants.END_POINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setRequestInterceptor(new MovieServiceInterceptor())
                .build();

        service = restAdapter.create(MovieService.class);
    }


}
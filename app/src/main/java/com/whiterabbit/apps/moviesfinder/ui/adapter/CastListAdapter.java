package com.whiterabbit.apps.moviesfinder.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.whiterabbit.apps.moviesfinder.R;
import com.whiterabbit.apps.moviesfinder.model.Cast;
import com.whiterabbit.apps.moviesfinder.network.MovieServiceConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tania Pinheiro
 *
 * Adapter that serves a cast list
 */
public class CastListAdapter extends BaseAdapter {

    private final Context context;
    private final LayoutInflater inflater;
    private final String baseUrl;
    private List<Cast> list;

    public CastListAdapter(Context context, ArrayList<Cast> castList) {
        this.list = castList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        baseUrl = MovieServiceConstants.IMAGES_END_POINT;
    }

    /**
     * @return the number of cast members in the list
     */
    @Override
    public int getCount() {
        return list.size();
    }


    /**
     * @param position of the selected page
     * @return the object in the given position
     */
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }


    //ViewHolder object to store each of the component views
    public static class ViewHolder {

        public TextView name;
        public TextView character;
        public ImageView poster;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        //if the view wasn't created yet, setup view
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_row_cast_list, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) rowView.findViewById(R.id.cast_name);
            viewHolder.character = (TextView) rowView.findViewById(R.id.cast_character);
            viewHolder.poster = (ImageView) rowView.findViewById(R.id.cast_poster);
            rowView.setTag(viewHolder);
        }
        //populate view with data
        final ViewHolder holder = (ViewHolder) rowView.getTag();
        Cast cast = (Cast) getItem(position);
        if (cast != null) {
            holder.name.setText(cast.getName());
            holder.character.setText(cast.getCharacter());
            Picasso.with(context).load(baseUrl + cast.getProfile_path()).into(holder.poster);
        }
        return rowView;
    }
}

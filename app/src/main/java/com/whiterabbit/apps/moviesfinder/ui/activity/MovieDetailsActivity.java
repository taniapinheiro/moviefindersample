package com.whiterabbit.apps.moviesfinder.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.whiterabbit.apps.moviesfinder.AppApplication;
import com.whiterabbit.apps.moviesfinder.R;
import com.whiterabbit.apps.moviesfinder.model.Movie;
import com.whiterabbit.apps.moviesfinder.network.MovieServiceConstants;
import com.whiterabbit.apps.moviesfinder.ui.adapter.TabsViewPagerAdapter;
import com.whiterabbit.apps.moviesfinder.ui.SlidingTabLayout;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Tania Pinheiro.
 *
 * This activity show the details of a specific movie.
 * It contains a sliding tab layout
 */

public class MovieDetailsActivity extends ActionBarActivity {

    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    private ImageView moviePoster;
    private TextView movieTagline;
    private Movie movie;
    private String movieId;
    private Toolbar toolbar;
    private TabsViewPagerAdapter tabsViewPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        //setup toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //setup views
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        moviePoster = (ImageView) findViewById(R.id.movie_backdrop);
        movieTagline = (TextView) findViewById(R.id.movie_title);

        //request movie data
        movieId = getIntent().getStringExtra(Intent.EXTRA_UID);
        loadDataFromApi();

        }

    private void loadDataFromApi() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                AppApplication.getMoviesService().getMovie(movieId, new retrofit.Callback<Movie>() {
                    @Override
                    public void success(Movie newMovie, Response response) {
                        movie = newMovie;

                        //request succeeded, update ui
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //populate views with data
                                populateViews();

                                //setup of the sliding tab layout
                                //the fragments in the viewpager need to know which movie they refer
                                tabsViewPagerAdapter = new TabsViewPagerAdapter(MovieDetailsActivity.this, getSupportFragmentManager(), movie);
                                mViewPager.setAdapter(tabsViewPagerAdapter);
                                mSlidingTabLayout.setViewPager(mViewPager);
                                mSlidingTabLayout.setDistributeEvenly(true);

                                //set tab indicator color
                                mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

                                    @Override
                                    public int getIndicatorColor(int position) {
                                        return getResources().getColor(R.color.primary_color_light);
                                    }

                                });
                                tabsViewPagerAdapter.notifyDataSetChanged();
                            }
                        });

                    }
                    @Override
                    public void failure(RetrofitError error) {
                        //there was an error, notify the ui
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MovieDetailsActivity.this, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });

            }
        }).start();
    }

    private void populateViews() {
        getSupportActionBar().setTitle(movie.getTitle());
        if (movie.getTagline().isEmpty())
            movieTagline.setVisibility(View.GONE);
        movieTagline.setText(movie.getTagline());
        Picasso.with(this)
                .load(MovieServiceConstants.IMAGES_END_POINT + movie.getBackdrop_path())
                .into(moviePoster, imageCallback);
    }


    //A callback to set the background color of the movie tagline
    private Callback imageCallback =  new Callback() {
        @Override
        public void onSuccess() {
            //get bitmap from moviePoster
            Bitmap bitmap = ((BitmapDrawable) moviePoster.getDrawable()).getBitmap();

            //Asynchronously get the darkMutedColor from the bitmap using the Palette library
            Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
                @Override
                public void onGenerated(Palette palette) {
                    movieTagline.setBackgroundColor(palette.getDarkMutedColor(getResources().getColor(R.color.black_56)));
                }
            });
        }

        @Override
        public void onError() {
            //couldn't get image, setting fallback color
            moviePoster.setBackgroundColor(getResources().getColor(R.color.black_56));

        }
    };
}




package com.whiterabbit.apps.moviesfinder.network;

/**
 * Created by Tania Pinheiro
 *
 * This class contains the constants related to the requests
 */
public class MovieServiceConstants {

    //The url end point of themoviedb
    public static final String END_POINT = "https://api.themoviedb.org/3";

    //The api key of tmdb
    public static final String API_KEY = "api_key";
    public static final String API_KEY_VALUE = "your_api_key";

    //Image endpoint url with specific size
    public static final String IMAGES_END_POINT = "https://image.tmdb.org/t/p/w780/";
}

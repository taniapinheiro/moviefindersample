package com.whiterabbit.apps.moviesfinder.model;

import java.util.List;

/**
 * Created by Tania Pinheiro
 *
 * This class (POJO) represents the request result of the movie credits
 */
public class ResultsMovieCredits {

     private String id;
     private List<Cast> cast;

    public List<Cast> getCast ()
    {
        return cast;
    }
}

package com.whiterabbit.apps.moviesfinder.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.whiterabbit.apps.moviesfinder.AppApplication;
import com.whiterabbit.apps.moviesfinder.R;
import com.whiterabbit.apps.moviesfinder.model.Cast;
import com.whiterabbit.apps.moviesfinder.model.Movie;
import com.whiterabbit.apps.moviesfinder.ui.adapter.CastListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tania Pinheiro
 *
 * This fragment display the cast members of a specific movie
 * through a ListView
 */
public class CastFragment extends Fragment {

    private static final String KEY_MOVIE_ID = "movieID";
    private ArrayList<Cast> castList;
    private CastListAdapter mAdapter;
    private ListView mListview;

    // newInstance constructor for creating fragment with arguments
    public static CastFragment newInstance(Movie movie) {
        CastFragment fragment = new CastFragment();
        Bundle args = new Bundle();
        if (movie != null) {
            args.putString(KEY_MOVIE_ID, movie.getId());
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_cast, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //init view
        castList = new ArrayList<Cast>();
        mListview = (ListView) view.findViewById(R.id.listview);
        View mEmpty = view.findViewById(android.R.id.empty);
        mAdapter = new CastListAdapter(getActivity(), castList);
        mListview.setAdapter(mAdapter);
        mListview.setEmptyView(mEmpty);
        loadDataFromApi(getArguments().getString(KEY_MOVIE_ID));

    }

    //Request the cast members of the given movie
    private void loadDataFromApi(final String movieId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Cast> result = AppApplication.getMoviesService().getCredits(movieId).getCast();
                castList.addAll(result);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                     mAdapter.notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }
}

package com.whiterabbit.apps.moviesfinder.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.whiterabbit.apps.moviesfinder.R;
import com.whiterabbit.apps.moviesfinder.model.Movie;
import com.whiterabbit.apps.moviesfinder.ui.fragment.CastFragment;
import com.whiterabbit.apps.moviesfinder.ui.fragment.OverviewFragment;
import com.whiterabbit.apps.moviesfinder.ui.fragment.SimilarMoviesFragment;

/**
 * Created by Tania Pinheiro
 *
 * Adapter that serves the viewpager and the sliding tab layout,
 * It defines 3 tabs: Overview, Cast and Similar Movies
 */
public class TabsViewPagerAdapter extends FragmentPagerAdapter {

    private static final int N_TABS = 3;
    private final Context context;

    private Movie movie;


    public TabsViewPagerAdapter(Context context, FragmentManager fm, Movie movie){
        super(fm);
        this.movie = movie;
        this.context = context;
    }
    /**
     * @return the number of pages to display
     */
    @Override
    public int getCount() {
        return N_TABS;
    }



    /**
     * @param position of the selected page
     * @return the title of the selected page
     */
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.tab_title_overview);
            case 1:
                return context.getString(R.string.tab_title_cast);
            case 2:
                return "Similar Movies";
            default:
                return null;
        }
    }

    /**
     * @param position of the selected page
     * @return the fragment of the selected page
     */
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return OverviewFragment.newInstance(movie);
            case 1: 
                return CastFragment.newInstance(movie);
            case 2: 
                return SimilarMoviesFragment.newInstance(movie);
            default:
                return null;
        }
    }

}
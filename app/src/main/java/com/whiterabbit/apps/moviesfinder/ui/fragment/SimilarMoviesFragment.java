package com.whiterabbit.apps.moviesfinder.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.whiterabbit.apps.moviesfinder.AppApplication;
import com.whiterabbit.apps.moviesfinder.R;
import com.whiterabbit.apps.moviesfinder.model.Movie;
import com.whiterabbit.apps.moviesfinder.ui.adapter.MovieListAdapter;
import com.whiterabbit.apps.moviesfinder.ui.EndlessScrollListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tania Pinheiro
 *
 * This fragment display similar movies related to a specific movie
 * through a GridView
 */
public class SimilarMoviesFragment extends Fragment {


    private static final String KEY_MOVIE_ID = "movieID";
    private ArrayList<Movie> moviesList;
    private MovieListAdapter mAdapter;
    private GridView mGridview;
    private String movieId;

    // newInstance constructor for creating fragment with arguments
    public static SimilarMoviesFragment newInstance(Movie movie) {
        SimilarMoviesFragment fragment = new SimilarMoviesFragment();
        Bundle args = new Bundle();
        if (movie != null) {
            args.putString(KEY_MOVIE_ID, movie.getId());
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_similar_movies, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //init views
        moviesList = new ArrayList<Movie>();
        mGridview = (GridView) view.findViewById(R.id.gridview);
        mAdapter = new MovieListAdapter(getActivity(), moviesList);
        mGridview.setAdapter(mAdapter);
        movieId = getArguments().getString(KEY_MOVIE_ID);

        //add endless scroll listener
        mGridview.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadDataFromApi(movieId,page);
            }
        });

        //request the first page of data
        loadDataFromApi(movieId, 1);

    }

    private void loadDataFromApi(final String movieId, final int page) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //request a page of movies and add the result to the list of movies
                List<Movie> result = AppApplication.getMoviesService().getSimilarMovies(movieId, page).getMovies();
                moviesList.addAll(result);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }
}


package com.whiterabbit.apps.moviesfinder.network;

import retrofit.RequestInterceptor;

/**
 * Created by Tania Pinheiro
 *
 * This class intercepts all the requests and adds the api key to the query params
 *
 */
public class MovieServiceInterceptor implements RequestInterceptor {

    @Override public void intercept(RequestFacade request) {
        request.addQueryParam(MovieServiceConstants.API_KEY, MovieServiceConstants.API_KEY_VALUE);
    }
}

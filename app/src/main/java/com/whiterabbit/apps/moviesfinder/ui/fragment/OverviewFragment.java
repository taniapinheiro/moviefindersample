package com.whiterabbit.apps.moviesfinder.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whiterabbit.apps.moviesfinder.R;
import com.whiterabbit.apps.moviesfinder.model.Genre;
import com.whiterabbit.apps.moviesfinder.model.Movie;

/**
 * Created by Tania Pinheiro
 *
 * This fragment displays basic info about a specific movie
 */
public class OverviewFragment extends Fragment{

    private TextView movieOverview;
    private TextView movieGenres;
    private TextView movieDuration;
    private static String KEY_OVERVIEW = "movie_overview";
    private static String KEY_DURATION = "movie_duration";
    private static String KEY_GENRES = "movie_genres";


    // newInstance constructor for creating fragment with arguments
    public static OverviewFragment newInstance(Movie movie) {
        OverviewFragment fragmentFirst = new OverviewFragment();
        Bundle args = new Bundle();
        if (movie != null) {
            args.putString(KEY_OVERVIEW, movie.getOverview());
            args.putString(KEY_DURATION, movie.getRuntime() + "min");
            args.putString(KEY_GENRES, getFormattedGenres(movie.getGenres()));
            fragmentFirst.setArguments(args);
        }
        return fragmentFirst;
    }

    /**
     * @param genres list
     * @return genre names separated by a comma
     */
    private static String getFormattedGenres(Genre[] genres) {
        String[] genreNames = new String[genres.length];
        for (int i = 0; i< genreNames.length; i++){
            genreNames[i] = genres[i].getName();
        }
        return TextUtils.join(", ", genreNames);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_overview, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //setup views
        movieOverview = (TextView) view.findViewById(R.id.movie_overview);
        movieGenres = (TextView) view.findViewById(R.id.movie_genre);
        movieDuration = (TextView) view.findViewById(R.id.movie_duration);

        //populate views
        Bundle args = getArguments();

        movieOverview.setText(args.getString(KEY_OVERVIEW));
        movieDuration.setText(args.getString(KEY_DURATION));
        movieGenres.setText(args.getString(KEY_GENRES));
    }

}
